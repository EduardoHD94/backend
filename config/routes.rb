Rails.application.routes.draw do 
  resources :historials
  get 'urls/new'
  get 'urls/create'
  get '/:id' => "shortener/shortened_urls#show"
  resources :users
  post 'authenticate', to: 'authentication#authenticate'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
