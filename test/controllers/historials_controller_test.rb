require 'test_helper'

class HistorialsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @historial = historials(:one)
  end

  test "should get index" do
    get historials_url, as: :json
    assert_response :success
  end

  test "should create historial" do
    assert_difference('Historial.count') do
      post historials_url, params: { historial: { tinyurl: @historial.tinyurl, url: @historial.url, userid: @historial.userid } }, as: :json
    end

    assert_response 201
  end

  test "should show historial" do
    get historial_url(@historial), as: :json
    assert_response :success
  end

  test "should update historial" do
    patch historial_url(@historial), params: { historial: { tinyurl: @historial.tinyurl, url: @historial.url, userid: @historial.userid } }, as: :json
    assert_response 200
  end

  test "should destroy historial" do
    assert_difference('Historial.count', -1) do
      delete historial_url(@historial), as: :json
    end

    assert_response 204
  end
end
