class HistorialsController < ApplicationController
  skip_before_action :authenticate_request, only: [:create, :show, :index]
  before_action :set_historial, only: [:show, :update, :destroy]

  # GET /historials
  def index
    @historials = Historial.all

    render json: @historials
  end

  # GET /historials/1
  def show
    redirect_to @historial.url
  end

  # POST /historials
  def create
    @historial = Historial.new(historial_params)
    puts "****************"
    if Historial.last
       @x = Historial.last.id + 1
     else
      @x = 1
    end

    @historial.tinyurl = "http:localhost:4000/historials/" + @x.to_s
    puts params[:url]
    puts "****************"
    if @historial.save
      render json: @historial, status: :created, location: @historial
    else
      render json: @historial.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /historials/1
  def update
    if @historial.update(historial_params)
      render json: @historial
    else
      render json: @historial.errors, status: :unprocessable_entity
    end
  end

  # DELETE /historials/1
  def destroy
    @historial.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_historial
      @historial = Historial.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def historial_params
      params.require(:historial).permit(:userid, :url, :tinyurl)
    end
end
