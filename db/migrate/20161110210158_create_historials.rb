class CreateHistorials < ActiveRecord::Migration[5.0]
  def change
    create_table :historials do |t|
      t.integer :userid
      t.string :url
      t.string :tinyurl

      t.timestamps
    end
  end
end
